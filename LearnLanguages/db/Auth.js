import * as firebase from "firebase";

export default class Auth {

	async getUser(id) {
		return firebase.database().ref(`/users/${id}`).once('value')
	}

	async getUsers() {
		return firebase.database().ref("/users").once('value')
    }
    
    sendMail(){
        firebase.auth().currentUser.sendEmailVerification().then(function() {
            console.log("MAIL SENT!");
        }).catch(function(error) {
            console.log("Error code: " + error.code);
            console.log("Error message: " + error.message);
        });
    }

	createUser(email, password){
		firebase.auth().createUserWithEmailAndPassword(email, password).then(userCredential => {	  
		  firebase.database().ref('users/' + userCredential.user.uid).set({
			Email: email,
			Password: password
		  }).then(success => {
            console.log('USER SIGNED UP SUCCESSFULLY!');
            this.sendMail();
		  });
		}).catch(function (error) {
			console.log("Error code: " + error.code);
            console.log("Error message: " + error.message);
		  });
    }

    signIn(email, password){
        firebase.auth().signInWithEmailAndPassword(email, password).then(function() {
            console.log("YOU SIGNED IN SUCCESSFULLY, WELCOME!");
            // this.props.navigation.navigate('Home');
        }).catch(function(error) {
            console.log("Error code: " + error.code);
            console.log("Error message: " + error.message);
        });
    }

    userProfile(){
        var user = firebase.auth().currentUser;
        if (user != null) {
            console.log(user.email);
        }
    }

    signOut(){
        firebase.auth().signOut().then(function() {
            console.log('Sign-out successful');
          }).catch(function(error) {
            console.log('An error occured');
            console.log("Error code: " + error.Code);
            console.log("Error message: " + error.message);
          });
    }
}