import React, {Component} from 'react';
import {AlertIOS, StyleSheet, Text, TextInput, TouchableHighlight, View} from 'react-native';

import {db} from "./firebase.config";

let addTheme = theme => {
	themesRef.push({
		name: theme
	});
};
let themesRef = db.ref('/themes');

export default class AddTheme extends Component {

	state = {
		name: '',
		themes: []
	};
	getValue(){
		firebase.database().ref("/users/"+ id).once('value').then(function(snapshot) {
			var username = (snapshot.val() && snapshot.val().username) || 'Anonymous';
			// ...
		});
	}
	componentDidMount() {
		themesRef.on('value', snapshot => {
			let data = snapshot.val();
			let themes = Object.values(data);
			this.setState({themes});

			console.log(themes)
		});
	}

	handleChange = e => {
		this.setState({
			name: e.nativeEvent.text
		});
	};
	handleSubmit = () => {
		addTheme(this.state.name);
		AlertIOS.alert('Theme saved successfully');
	};

	render() {
		return (
			<View style={styles.main}>
				<Text style={styles.title}>Add Theme</Text>
				<TextInput style={styles.themeInput} onChange={this.handleChange}/>
				<TouchableHighlight
					style={styles.button}
					underlayColor="white"
					onPress={this.handleSubmit}
				>
					<Text style={styles.buttonText}>Add</Text>
				</TouchableHighlight>
				{this.state.themes.map(theme => <Text>{theme.name}</Text>)}
			</View>
		);
	}
}

const styles = StyleSheet.create({
	main: {
		flex: 1,
		padding: 30,
		flexDirection: 'column',
		justifyContent: 'center',
		backgroundColor: '#6565fc'
	},
	title: {
		marginBottom: 20,
		fontSize: 25,
		textAlign: 'center'
	},
	themeInput: {
		height: 50,
		padding: 4,
		marginRight: 5,
		fontSize: 23,
		borderWidth: 1,
		borderColor: 'white',
		borderRadius: 8,
		color: 'white'
	},
	buttonText: {
		fontSize: 18,
		color: '#111',
		alignSelf: 'center'
	},
	button: {
		height: 45,
		flexDirection: 'row',
		backgroundColor: 'white',
		borderColor: 'white',
		borderWidth: 1,
		borderRadius: 8,
		marginBottom: 10,
		marginTop: 10,
		alignSelf: 'stretch',
		justifyContent: 'center'
	}
});