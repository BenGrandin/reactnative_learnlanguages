import Firebase from 'firebase';

const config = {
	apiKey: "AIzaSyArak6so_FNz_6LqaPlfWYg2CNF6RzwsBA",
	authDomain: "learnlanguages-bf5f2.firebaseapp.com",
	databaseURL: "https://learnlanguages-bf5f2.firebaseio.com",
	projectId: "learnlanguages-bf5f2",
	storageBucket: "learnlanguages-bf5f2.appspot.com",
	messagingSenderId: "600912857901",
	appId: "1:600912857901:web:26dda3a307456621ee61a4",
	measurementId: "G-0E2MXSGPBR"
};

let app = Firebase.initializeApp(config);
export const db = app.database();