import React from 'react'
import {Text, View, Button} from 'react-native';
import { StyleSheet, TouchableOpacity} from 'react-native';
import CustomNavigationButton from './../components/CustomNavigationButton'
import Auth from './../db/Auth.js'
import CustomImput from './../components/CustomImput'
import { TextInput } from 'react-native-gesture-handler';
import * as firebase from "firebase";

class CreateAccountPage extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            id: '',
            password: ''
        }
    }

    auth = new Auth();

    idChange = (change) => {
        this.setState({id: change})
    }

    passWordChange = (change) => {
        this.setState({password: change})
    }

    createAccount(){
        this.auth.createUser(this.state.id, this.state.password);

        var user = firebase.auth().currentUser;
        if (user != null) {
            this.props.navigation.navigate('Home');
        }
    }

    render(){
        return(
            <View>
                <Text style={{textAlign: "center", fontSize:20, marginTop: 50}}>Create your account</Text>
                <CustomImput onChange = {this.idChange} text = 'Id' content = {this.state.id} placeholder='write your id'></CustomImput>
                <CustomImput onChange = {this.passWordChange} text= 'Password' content= {this.state.password} placeholder='write your password'></CustomImput>

                {/* <CustomNavigationButton navigation={this.props.navigation} linkTo={"Home"} title="Créer mon compte" color="white"/> */}

                <Button title='Create my account' onPress={this.createAccount.bind(this)}/>
            </View>
        )
    }
}


export default CreateAccountPage