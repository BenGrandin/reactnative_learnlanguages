import React from 'react'
import {View} from 'react-native';
import LanguagesList from '../components/LanguagesList'
import Crud from "../db/Crud";

class ThemesPage extends React.Component {

	constructor(props) {
		super(props);


		this.state = {
			languages: [],
		};

		Crud.getValues("languages").then(languages => {
			this.setState({languages})
		});
	}

	render() {
		const {languages} = this.state;

		return (
			<View>
				<LanguagesList
					navigation={this.props.navigation}
					liste={languages.map(l => l.name)}
				/>
			</View>
		)
	}

}

export default ThemesPage