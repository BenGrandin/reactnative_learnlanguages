import React from 'react'
import {Text, View, TextInput, Button} from 'react-native';
import CustomImput from './../components/CustomImput'
import { StyleSheet, TouchableOpacity, Image} from 'react-native';
import CustomNavigationButton from './../components/CustomNavigationButton'
import { AuthSession } from 'expo';
import Auth from './../db/Auth.js'
import * as firebase from "firebase";
//import { Colors } from 'react-native/Libraries/NewAppScreen';

class LoginPage extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            id: '',
            password: ''
        }
    }

    auth = new Auth();

    idChange = (change) => {
        this.setState({id: change})
    }

    passWordChange = (change) => {
        this.setState({password: change})
    }

    connection(){
        // this.auth.signIn(this.state.id, this.state.password);

        var user = firebase.auth().currentUser;
        if (user != null){
            this.props.navigation.navigate('Home');
        }
        else{
            this.auth.signIn(this.state.id, this.state.password);
        }
    }
    
    render(){
        const { navigate } = this.props.navigation;
        return(
            <View> 
                {/* <Image source={{uri: 'https://stickeramoi.com/15895-large_default/sticker-drapeaux-du-monde-au-choix-4-cm-sur-26-cm.jpg'}}
                style={{width: 400, height: 190}} /> */}

                <Text style={{fontSize: 49, textAlign: "center", marginTop: 40}}>Sign In</Text>

                <CustomImput onChange = {this.idChange} text = 'id' content = {this.state.id} ></CustomImput>
                <CustomImput onChange = {this.passWordChange} text= 'password' content= {this.state.password}></CustomImput>

                {/* <CustomNavigationButton navigation={this.props.navigation} linkTo="Home" title="Sign In" color="white"/> */}
                <Button title='Sign In' onPress={this.connection.bind(this)}/>
                <CustomNavigationButton navigation={this.props.navigation} linkTo="CreateAccount" title="Create my account" color="white"/>
            </View>
        )
    }

}

const styles = StyleSheet.create({
    button: {
      marginTop: 60,
      backgroundColor: "#00b5ec",
      borderColor: 'white',
      borderWidth: 1,
      borderRadius: 20,
      color: 'white',
      fontSize: 20,
      overflow: 'hidden',
      padding: 10,
      textAlign:'center',
      flexDirection: "row",
      width: "70%",
      left: "50%"
    }
  });

export default LoginPage