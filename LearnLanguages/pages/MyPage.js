import React from 'react'
import {Text, View, Button, StyleSheet, Image} from 'react-native';
import CustomNavigationButton from '../components/CustomNavigationButton'

class MyPage extends React.Component{
_onPressButton() {
    alert('You tapped the button!')
  }

    render(){

     const {navigation} = this.props;
        return(
           
            <View style={styles.container}>
                <View style={styles.alternativeLayoutButtonContainer}>
                  <Text>
                   "PseudoName!"</Text>

                </View>

                <Image
                  style={{width: 150, height: 150}}
                  source={require('../images/personat.png')}
                />

              <CustomNavigationButton
                title="Création de fiche"
                linkTo = 'CreateCard'
                navigation = {navigation}
                color= 'lightblue'>

              </CustomNavigationButton>

              <CustomNavigationButton
                title="Réglages"
                linkTo = 'Reglage'
                navigation = {navigation}
                color= 'red'>

              </CustomNavigationButton>

              <CustomNavigationButton
                title="Thêmess"
                linkTo = 'Themes'
                navigation = {navigation}
                color= 'green'>
                
              </CustomNavigationButton>

              <CustomNavigationButton
                title="Go aux exercices"
                linkTo = 'Home'
                navigation = {navigation}
                color= 'pink'>
                
              </CustomNavigationButton>
            </View>
        )
    }

}
const styles = StyleSheet.create({
  container: {
   flex: 1,
   justifyContent: 'center',
  },
  alternativeLayoutButtonContainer: {
    margin: 20,
    flexDirection: 'row',
    justifyContent: 'space-between'
  }
});
export default MyPage

