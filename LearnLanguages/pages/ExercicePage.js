import React from 'react'
import {Text, TextInput, View} from 'react-native';
import Crud from "../db/Crud";

class ExercicePage extends React.Component {

	constructor(props) {
		super(props);

		const {navigation} = props

		const language = navigation.getParam("language");

		this.state = {
			guesses: {},
			name: language.name,
			languages: null
		};


		Crud.getValues("languages", {key: "languages", value: this.state.name}).then(languages => {
			console.log(languages)
			this.setState({languages})
		});
	}

	handleChange = (tradInput, key) => {
		let guesses = this.state.guesses;

		guesses[key] = tradInput;
		this.setState(
			{
				guesses
			}
		)
	};

	render() {


		const {navigation} = this.props;

		const {guesses, name, languages} = this.state;
		let fiches;

		if (languages === null) {
			fiches = <Text>Chargement...</Text>

		} else if (languages.length === 0) {
			fiches = <Text>Aucune fiche</Text>
			
		} else if (languages && languages.length) {
			fiches = languages.definition.map((element, key) => {
				return (
					<View key={key} style={{flexDirection: 'row'}}>
						<Text>Quelle est la traduction de: </Text>
						<Text style={{margin: 5, backgroundColor: 'skyblue', fontSize: 20}}>{element.content}</Text>
						<TextInput onChangeText={(guess) => this.handleChange(guess, key)}
								   style={{height: 20, margin: 5, width: 100, backgroundColor: 'red'}}
								   value={this.state.guesses[key]}/>

					</View>
				)
			})
		}

		return (
			<View>{fiches}
				<Text>{JSON.stringify(this.state.guesses)}</Text>
			</View>)
	}

}

export default ExercicePage