import React from 'react'

import {Text, TextInput, View} from 'react-native';
import Crud from "../db/Crud";


class ThemesPage extends React.Component {

	constructor(props) {
		super(props);

		const {navigation} = props;

		const title = navigation.getParam("title");

		this.state = {
			definitions: null,
			guesses: {}
		};

		Crud.getValues("definitions", {key: "language", value: title}).then(definitions => {
			this.setState({definitions})
		});
	}

	handleChange(guess, key) {
		const {guesses} = this.state;
		console.log({guess, key});

		guesses[key] = guess;
		this.setState(guesses)
	}

	render() {

		const {navigation} = this.props;
		const {definitions} = this.state;
		let fiches;

		if (definitions === null) {
			fiches = <Text>Chargement...</Text>

		} else if (definitions.length === 0) {
			fiches = <Text>Aucune fiche</Text>

		} else if (definitions && definitions.length) {
			fiches = definitions.map((element, key) => {
				return (
					<View key={key} style={{flexDirection: 'row'}}>
						<Text>Quelle est la traduction de: </Text>
						<Text style={{margin: 5, backgroundColor: 'skyblue', fontSize: 20}}>{element.content}</Text>
						<TextInput onChangeText={(guess) => this.handleChange(guess, key)}
								   style={{height: 20, margin: 5, width: 100, backgroundColor: 'red'}}
								   value={this.state.guesses[key]}/>

					</View>
				)
			})
		}

		return (
			<View>
				{fiches}
			</View>
		)
	}

}

export default ThemesPage