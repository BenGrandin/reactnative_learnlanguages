import React from 'react'
import {ScrollView, StyleSheet, Text, TouchableOpacity} from 'react-native';

export default function ThemeCardsCreator(props) {

	const {languages, navigation} = props;

	const cardCreation = () => {

		return (languages.length
			? languages.map(language => (
				<TouchableOpacity
					style={styles.button}
					key={language.name}
					onPress={() => navigation.navigate('Exercice', {language})}

				>
					<Text>{language.name}</Text>
				</TouchableOpacity>
			))
			: <Text>Chargement..</Text>)
	};


	return (
		<ScrollView>
			{cardCreation()}
		</ScrollView>)
}

const styles = StyleSheet.create({
	button: {
		flex: 1,
		alignItems: 'stretch',
		flexDirection: 'column',
		backgroundColor: '#FFA114',
		margin: 10,
		height: 100,
		width: 100
	}
});
