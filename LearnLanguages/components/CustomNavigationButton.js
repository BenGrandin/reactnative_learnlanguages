import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';


const CustomNavigationButton = (props) => {

	const {title, color, linkTo, navigation, params} = props;

	const styles = StyleSheet.create({
		button: {
			marginTop: 30,
			backgroundColor: "#00b5ec",
			borderColor: 'white',
			borderWidth: 1,
			borderRadius: 20,
			fontSize: 20,
			overflow: 'hidden',
			padding: 10,
			flexDirection: "row",
			width: "70%",
			left: "50%"
		},
		text: {
			color,
			fontSize: 20,
			textAlign: "center",
		}
	});

	return (
		<View>
			<TouchableOpacity
				onPress={() => navigation.navigate(linkTo, params)}
				style={styles.button}>
				<Text style={styles.text}>{title}</Text>
			</TouchableOpacity>
		</View>
	)


};


export default CustomNavigationButton