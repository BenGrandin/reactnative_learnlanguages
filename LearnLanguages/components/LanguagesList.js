import React from 'react'
import {View} from 'react-native';
import CustomNavigationButton from './CustomNavigationButton'


const LanguagesList = (props) => {

	const {navigation, liste} = props;

	const listCreation = () => {
		return (liste.map((item, key) => (

				<CustomNavigationButton
					key={key}
					title={item}
					color='red'
					linkTo='ShowThemes'
					navigation={navigation}
					params={{title: item}}/>
			)
		))
	};
	return (
		<View>
			{listCreation()}
		</View>
	)
};
export default LanguagesList