import React from 'react'
import {Text, TextInput, View} from 'react-native'

const CustomImput = (props) => {

    const {onChange, content, text} = props
    const handleChange = (event) => {
        onChange(event)
    }
    return(
        <View style={{marginTop: 50}}>
                <Text style={{marginLeft: 30, marginBottom: 4, fontSize: 18}}>{text}</Text>
                <TextInput
                style={{height: 40, width: 300, marginLeft: 30,borderWidth: 2, borderColor: 'lightblue', paddingLeft: 6, borderRadius: 20}}
                onChangeText={handleChange}
                value= {content}
					/>
                

        </View>
    )
}

export default CustomImput